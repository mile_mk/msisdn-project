<?php
use JsonRPC\Client;

$loader = require 'vendor/autoload.php';
$loader->add('JsonRPC', __DIR__.'/../src/');

// define variables and set to empty values
$validationErr = '';
$msisdn = '';
$subscriber_number='';
$country_id='';
$prefix_id='';
$country_code='';
$operator_name='';
$area_name='';
$error='';

if (isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "POST") {
	if (empty($_POST["msisdn"])) {
		$validationErr = "MSISDN is required";
	} else {
		try {
			$msisdn = $_POST["msisdn"];
			$msisdn_info = get_msisdn_info($msisdn);

		} catch (Exception $e) {
			$error = 'Caught exception: '.$e->getMessage();
			}
		
		if(!empty($msisdn_info)){
				$subscriber_number=$msisdn_info['subscriber_number'];
				$country_id=$msisdn_info['country_id'];
				$prefix_id=$msisdn_info['prefix_id'];
				$country_code=$msisdn_info['country_code'];
				
				if($msisdn_info['operator_name']!=''){
					$operator_name='('.$msisdn_info['operator_name'].')';
				}
				
				if($msisdn_info['area_name']!=''){
					$area_name='('.$msisdn_info['area_name'].')';
				}
			}
	}
}

?>

<html>
	<head>
		<style>
			.error {color: #FF0000;}
			.main-div {width:20%;
						margin-left:42%;
						margin-top:3%;
						font-size: 16px;}
			.button-submit {text-align: center;
							text-decoration: none;
							display: inline-block;
							font-size: 15px;
							width:25%;
							min-width:60px;}
		</style>
	</head>
	<body>  
		<h2 style="text-align:center;margin-top:5%;">MSISDN INFORMATION</h2>
		<div  class="main-div">
			<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">  
				<span>Enter MSISDN:</span>
				<br />
				<input type="text" name="msisdn" value="<?php echo isset($_POST["msisdn"]) ? $_POST["msisdn"] : '';?>">
				<br />
				<span class="error"><?php echo $validationErr;?></span>
			  <br /><br />
			  <input type="submit" class="button-submit" name="submit" value="Submit">  
			</form>
			<br />

			<?php
			if(!empty($msisdn)){
				if(isset($msisdn_info) && $subscriber_number!=''){
					echo "<h3>Your Input: $msisdn</h3>";
					echo (!empty($area_name)) ? "<h4>Area code: $prefix_id $area_name</h4>" : "<h4>MNO identifier: $prefix_id $operator_name</h4>";
					echo "<h4>Country dialling code: +$country_code</h4>
						<h4>Subscriber number: $subscriber_number</h4>
						<h4>Country identifier (ISO 3166-1-alpha-2): $country_id</h4>";
				} else {
					if(!empty($error)){
						echo "<h4>Unsuccessful request</h4>";
					} else{
						echo "<h4>Invalid input: $msisdn</h4>";
					}
				}
			}?>
		</div>
	</body>
</html>

<?php
	function get_msisdn_info($msisdn){
  
		$msisdn_info=array();
		if(is_numeric($msisdn) && strlen($msisdn)>5){
			
			$client = new Client('http://project.dev/api/msisdn_api');
			$client->getHttpClient()->withDebug();
				
			$client->getHttpClient()
			->withUsername('user')
			->withPassword('password');
			$result = $client->execute('msisdn_info', [$msisdn]);
				
			$msisdn_info=json_decode($result,true);
			$msisdn_info=$msisdn_info['response'];
		}
		
		return $msisdn_info;
	}
	function test_input($data) {
	  $data = trim($data);
	  $data = stripslashes($data);
	  $data = htmlspecialchars($data);
	  $data = str_replace(' ','',$data);
	  return $data;
}
?>