<?php
use JsonRPC\Client;
require_once __DIR__.'/../vendor/autoload.php';
class ClientTest extends PHPUnit_Framework_TestCase
{
    private $httpClient;
    public function setUp()
    {
        $this->httpClient = $this
            ->getMockBuilder('\JsonRPC\HttpClient')
            ->setMethods(array('execute'))
            ->getMock();
    }
    
    public function testSendRequest()
    {
        $client = new Client('', false, $this->httpClient);
        $this->httpClient
            ->expects($this->once())
            ->method('execute')
            ->with($this->stringContains('{"jsonrpc":"2.0","method":"get_msisdn_info","id":'))
            ->will($this->returnValue(array('jsonrpc' => '2.0', 'result' => '{"response":[]}', 'id' => 1)));
        $result = $client->execute('get_msisdn_info', array('+38975423364'));
        $this->assertEquals($result, '{"response":[]}');
    }
    public function testSendRequestWithError()
    {
        $client = new Client('', false, $this->httpClient);
        $this->httpClient
            ->expects($this->once())
            ->method('execute')
            ->with($this->stringContains('{"jsonrpc":"2.0","method":"get_msisdn_info","id":'))
            ->will($this->returnValue(array(
                'jsonrpc' => '2.0',
                'error' => array(
                    'code' => -32601,
                    'message' => 'Method not found',
                ),
            )));
        $this->setExpectedException('BadFunctionCallException');
        $client->execute('get_msisdn_info', array('+38975423364'));
    }
    public function testSendRequestWithErrorAndReturnExceptionEnabled()
    {
        $client = new Client('', true, $this->httpClient);
        $this->httpClient
            ->expects($this->once())
            ->method('execute')
            ->with($this->stringContains('{"jsonrpc":"2.0","method":"get_msisdn_info","id":'))
            ->will($this->returnValue(array(
                'jsonrpc' => '2.0',
                'error' => array(
                    'code' => -32601,
                    'message' => 'Method not found',
                ),
            )));
        $result = $client->execute('get_msisdn_info', array('+38975423364'));
        $this->assertInstanceOf('BadFunctionCallException', $result);
    }
	
	 /**
     * @dataProvider providerTestValidMsisdn
     */
	 public function testValidMsisdn($input,$expected_result){
		 $client = new Client('http://project.dev/api/msisdn_api');
		 $client->getHttpClient()
			->withUsername('user')
			->withPassword('password');
			
		 $result = $client->execute('msisdn_info', $input);
		
		 $this->assertEquals($result, $expected_result);
	 }
	 
	  public function providerTestValidMsisdn(){
		  return array(
            array(array('+38975423364'),json_encode(array('response' => array(
																'prefix_id' =>'75',
																'country_code' => '389',
																'subscriber_number' => '423364',
																'country_id' => 'MK',
																'operator_name' => 'Cosmofon',
																'area_name' => '')))),
			array(array('+38978683182'),json_encode(array('response' => array(
																'prefix_id' =>'78',
																'country_code' => '389',
																'subscriber_number' => '683182',
																'country_id' => 'MK',
																'operator_name' => 'VIP Operator',
																'area_name' => '')))),
			array(array('+38640125266'),json_encode(array('response' => array(
																'prefix_id' =>'40',
																'country_code' => '386',
																'subscriber_number' => '125266',
																'country_id' => 'SI',
																'operator_name' => 'Si.mobil',
																'area_name' => '')))),
			array(array('+38162863951'),json_encode(array('response' => array(
																'prefix_id' =>'62',
																'country_code' => '381',
																'subscriber_number' => '863951',
																'country_id' => 'RS',
																'operator_name' => 'Telenor Serbia',
																'area_name' => '')))),
			array(array('+385997294761'),json_encode(array('response' => array(
																'prefix_id' =>'99',
																'country_code' => '385',
																'subscriber_number' => '7294761',
																'country_id' => 'HR',
																'operator_name' => 'T-Mobile',
																'area_name' => '')))),
			array(array('+18666581463'),json_encode(array('response' => array(
																'prefix_id' =>'866',
																'country_code' => '1',
																'subscriber_number' => '6581463',
																'country_id' => 'CA',
																'operator_name' => '',
																'area_name' => 'Non-geographic, toll-free')))),
			array(array('+19058637583'),json_encode(array('response' => array(
																'prefix_id' =>'905',
																'country_code' => '1',
																'subscriber_number' => '8637583',
																'country_id' => 'CA',
																'operator_name' => '',
																'area_name' => 'Ontario')))),
			array(array('+19009746287'),json_encode(array('response' => array(
																'prefix_id' =>'900',
																'country_code' => '1',
																'subscriber_number' => '9746287',
																'country_id' => 'CA',
																'operator_name' => '',
																'area_name' => 'Non-geographic, premium-rate')))),
			array(array('+14438236054'),json_encode(array('response' => array(
																'prefix_id' =>'443',
																'country_code' => '1',
																'subscriber_number' => '8236054',
																'country_id' => 'US',
																'operator_name' => '',
																'area_name' => 'Maryland')))),
			array(array('+521876345123'),json_encode(array('response' => array(
																'prefix_id' =>'1',
																'country_code' => '52',
																'subscriber_number' => '876345123',
																'country_id' => 'MX',
																'operator_name' => 'Telcel, Movistar, IUSACell, Nextel, UNEFON, Virgin Mobile, Tuenti',
																'area_name' => '')))),
			array(array('+46733497612'),json_encode(array('response' => array(
																'prefix_id' =>'733',
																'country_code' => '46',
																'subscriber_number' => '497612',
																'country_id' => 'SE',
																'operator_name' => 'Telenor Sverige AB',
																'area_name' => ''))))
        );
	  }
	  
	  /**
	 * @dataProvider providerTestInvalidMsisdn
     */
	 public function testInvalidMsisdn($input){
		 $client = new Client('http://project.dev/api/msisdn_api');
		 $client->getHttpClient()
			->withUsername('user')
			->withPassword('password');
		 $result = $client->execute('msisdn_info', $input);
		 $expecte_result=json_encode(array('response' => array()));
		 $this->assertEquals($result, $expecte_result);
	 }
	 
	 public function providerTestInvalidMsisdn(){
		  return array(
            array(array('+38995423364')),
			array(array('+389')),
			array(array('+98755423364')),
			array(array('38975423364')),
			array(array('mobile number'))
        );
	  }
	  
	 public function testEmptyInputArgument(){
		 $client = new Client('http://project.dev/api/msisdn_api');
		 $client->getHttpClient()
			->withUsername('user')
			->withPassword('password');
		 $result = $client->execute('msisdn_info', array(''));
		 $expecte_result=json_encode(array('response' => array()));
		 $this->assertEquals($result, $expecte_result);
	 }
	 
	 /**
     * @expectedException TypeError
	 * @dataProvider providerTestInvalidInputArgument
     */
	 public function testInvalidInputArgument($input){
		 $client = new Client('http://project.dev/api/msisdn_api');
		 $client->getHttpClient()
			->withUsername('user')
			->withPassword('password');
		 $result = $client->execute('msisdn_info', $input);
	 }
	 
	  public function providerTestInvalidInputArgument(){
		  return array(
            array('38995423364'),
			array('38995423364','38695423364'),
			array(''),
			array(12345),
			array(true)
        );
	  }
	  
	 /**
     * @expectedException InvalidArgumentException
	 * @dataProvider providerTestInvalidArgumentException
     */
	 public function testInvalidArgumentException($input){
		 $client = new Client('http://project.dev/api/msisdn_api');
		 $client->getHttpClient()
			->withUsername('user')
			->withPassword('password');
		$result = $client->execute('msisdn_info', $input);
		 
	 }
	 
	 public function providerTestInvalidArgumentException(){
		  return array(
            array(array('38995423364','38995423364'))
        );
	  }
	  
	 /**
     * @expectedException JsonRPC\Exception\InvalidJsonFormatException
	 * @dataProvider providerTestInvalid
     */
	 public function testInvalid($input){
		 $client = new Client('http://project.dev/api/msisdn_api');
		 $client->getHttpClient()
			->withUsername('user')
			->withPassword('password');
		$result = $client->execute('msisdn_info', $input);
		 
	 }
	 
	 public function providerTestInvalid(){
		  return array(
            array(array(array('38995423364','38995423364')))
        );
	  }
	  
	  /**
	 * @expectedException ArgumentCountError
	 * @dataProvider providerTestArgumentCountError
     */
	 public function testArgumentCountError($input,$username, $password){
		 $client = new Client('http://project.dev/api/msisdn_api');
		 $client->getHttpClient()
			->withUsername($username)
			->withPassword($password);
		$result = $client->execute('msisdn_info', $input);
		 
	 }
	 
	 public function providerTestArgumentCountError(){
		  return array(
            array(),
			array(array('+385997294761')),
			array(array('+385997294761'),'user')			
        );
	  }
	  
	  /**
	 * @expectedException JsonRPC\Exception\AccessDeniedException
	 * @dataProvider providerTestUnauthorized
     */
	 public function testUnauthorized($input,$username,$password){
		 $client = new Client('http://project.dev/api/msisdn_api');
		 $client->getHttpClient()
			->withUsername($username)
			->withPassword($password);
		$result = $client->execute('msisdn_info', $input);
		 
	 }
	 
	 public function providerTestUnauthorized(){
		  return array(
            array(array('+385997294761'),'',''),
			array(array('+385997294761'),'wrong_user','password'),
			array(array('+385997294761'),'user','wrongpassword'),
			array(array('+385997294761'),'wrong_user','wrongpassword')
        );
	  }
}