SETTING UP THE VIRTUAL MACHINE  
	For setting up the virtual machine (VM), execute the "vagrant up" command in a terminal in the root folder of the project (msisdn-project). Next, execute the "vagrant provision" command to provision the VM 
	with the custom puppet modules that are used. If the commands are successful, the VM is up and running.  
  
ADDING HOST  
	For the use of the domain name "project.dev", that is setup as virtual host, the computers host file needs to be altered by adding the following host: "192.168.56.101 project.dev www.project.dev".  
  
JSON-RPC API  
	The path of the API that takes a MSISDN as an input and returns information about it (MNO identifier, country dialing code, subscriber number and country identifier as defined with ISO 3166-1-alpha-2)
	is "/project.dev/api/msisdn_api". This API is based on the JSON-RPC protocol and uses the fguillot/JsonRPC client/server library ("https://github.com/fguillot/JsonRPC.git").
	The following JSON needs to be sent to the API: {"jsonrpc":"2.0","method":"msisdn_info","id":1,"params":[""]}, where "method":"msisdn_info" is the name of the method that is called and "params" is an array of input parameters for the method. 
	The method "msisdn_info" takes one input parameter which is a MSISDN (for example "params":["+38971123456"]). The API uses basic access authentication, therefore basic authorization header needs to be set for
	the request to be successful (valid credentials for testing are "user" for username and "password" for password). If the request is successful, JSON containing the result is sent back to the client, if not, 
	the returning JSON contains the error message.    
  
USING THE API  
	This API is used in a test php application, which can be accessed at http://192.168.56.101/index.php. In this application, a MSISDN is entered in the given input field, and after submitting the value, 
	a request to the API is made, using the entered value as parameter for the JSON that is sent to the API. The returned result is shown in the view.  
  
PHPunit TESTING  
	PHPUnit module is used for unit testing of the API. To run the PHPUnit tests, first connect to the vagrant host: 127.0.0.1, port: 2222, username: vagrant, private key: /msisdn-project/puphpet/files/dot/ssh/id_rsa, and 
	execute the command vendor/bin/phpunit tests, from the root folder of the APIm which is /var/www/project/api/. 
