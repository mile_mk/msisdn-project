class client_module {
	# execute 'apt-get update'
	  exec { 'apt-update':                    # exec resource named 'apt-update'
	  command => '/usr/bin/apt-get update'  # command this resource will run
	}

	# install json-rpc
	exec { "composer require":
	  command => 'composer require fguillot/json-rpc @stable -d /var/www/project/api',
	  environment => ["COMPOSER_HOME=/usr/local/bin"],
	  path    => '/usr/bin:/usr/local/bin:~/.composer/vendor/bin/',
	  timeout => 600
	}

}

class { 'gitlab':
  external_url => 'http://gitlab.mydomain.tld',
}

# Install with defaults
class { 'php::phpunit':

}