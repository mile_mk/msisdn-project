class server_module {
	
	# execute 'apt-get install zip unzip'
	  exec { 'apt-install':                    # exec resource named 'apt-install'
	  command => '/usr/bin/apt-get install zip unzip'  # command this resource will run
	}

	
	# install json-rpc
	exec { "composer require":
	  command => 'composer require fguillot/json-rpc @stable -d /var/www/html; composer require fguillot/json-rpc @stable -d /var/www/project/api',
	  environment => ["COMPOSER_HOME=/usr/local/bin"],
	  path    => '/usr/bin:/usr/local/bin:~/.composer/vendor/bin/',
	  timeout => 1200
	}
}


# Install with defaults
class { 'php::phpunit':

}